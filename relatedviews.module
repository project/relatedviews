<?php

/**
 * Implementation of hook_views_tables.
 * Declares information that this module provides for views integration.  In
 * our case, that means defining the filter.
 */
function relatedviews_views_tables() {
  if (module_exists('taxonomy')) {
    $vocab_options = array();
    foreach (taxonomy_get_vocabularies() as $vocab) {
      $vocab_options[$vocab->vid] = $vocab->name;
    } // foreach vocabulary

    $tables["term_from_curnode"] = array(
      'name' => 'term_from_curnode',
      'provider' => 'internal',
      'join' => array(
        'left' => array(
          'table' => 'node',
          'field' => 'nid'
        ),
        'right' => array(
          'field' => 'nid'
        )
      ),
      'filters' => array(
        'tid' => array(
          'name' => t('Taxonomy: Term from current node'),
          'value-type' => 'array', 
          'operator' => array('=' => t('Is From')),
          'handler' => 'relatedviews_handler_filter_taxcurnode',
          'value' => array(
             '#type' => 'select',
             '#options' => $vocab_options,
          ),
          'option' => 'string',
          'help' => t('This will look at the node currently being displayed and get the (first) taxonomy term from the vocabulary specified here under "option".'),
        ),
      )
    );

    return $tables;
  } // if taxonomy module
} // function relatedviews_views_tables


/**
 * Handles the 'taxonomy term of current node' view argument.
 * 1. Get the current term.
 * 2. Pass it off to the normal handler.
 */
function relatedviews_handler_filter_taxcurnode(
    $op, $filter, $filterinfo, &$query) 
{
  $nid = NULL;
  $vid = NULL;
  if (arg(0) == 'node' && is_numeric(arg(1))) {
    $nid = arg(1);
  } // if there's an nid to get

  $term = null;
  $vid = $filter['value'][0];
  if (!is_null($nid)) {
    $terms = array_keys(taxonomy_node_get_terms_by_vocabulary($nid, $vid));
    $term = $terms[0];
  }

  if (is_null($term)) {
    if (!empty($filter['options'])) {
      $terms = taxonomy_get_term_by_name($filter['options']);
      foreach ($terms as $myterm) {
          if ($myterm->vid == $vid)
              $term = $myterm->tid;
          break;
      }
    } else {
      // No term and no default = no results.
      $query->where[] = "0";
    }
  }

  _views_add_taxonomy($filter['operator'], array($term), 0, $query);

  $query->where[] = "node.nid != '%s'";
  $query->where_args[] = $nid;

} // function relatedviews_handler_filter_taxcurnode
